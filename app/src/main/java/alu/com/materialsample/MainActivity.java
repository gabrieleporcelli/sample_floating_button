package alu.com.materialsample;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Scene;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.transition.TransitionManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {

    public static final int USER_NUMBER = 16;
    private Scene mScene1;
    private Scene mScene2;
    private Transition transitionMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();

        //init transitions
        ViewGroup rootContainer = (ViewGroup) findViewById(R.id.root);
        transitionMgr = TransitionInflater.from(this).inflateTransition(R.transition.transition);
        mScene1 = Scene.getSceneForLayout(rootContainer, R.layout.scene_1, this);
        mScene2 = Scene.getSceneForLayout(rootContainer, R.layout.scene_2, this);
        mScene1.enter();

        //populate the listView
        populateListView();
    }


    public void goToScene1(View view) {
        TransitionManager.go(mScene1, transitionMgr);
        populateListView();
    }

    public void goToScene2(View view) {
        TransitionManager.go(mScene2, transitionMgr);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);
    }

    private void populateListView() {
        ListView listView = (ListView) findViewById(R.id.sample_listview);
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i <= USER_NUMBER; i++) {
            arrayList.add("User " + i);
        }
        String[] arrayOfStrings = arrayList.toArray(new String[arrayList.size()]);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayOfStrings);
        listView.setAdapter(arrayAdapter);
    }
}
